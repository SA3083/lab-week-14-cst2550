#include <iostream>
#include <ostream>

template <typename T,int maxSize>
class List{

private:
  T *data;
  int size;
public:
  
  List()
  {
    this->data = new T[100];
    this->size = 0;
  }

  void add(T item)
  {
    this->data[this->size++]=item;
  }

  T get(int pos)
  {
    return this->data[pos];
  }
 
  T &operator[](int pos)
  {
    return this->data[pos];
  }

  void remove(int pos)
  {
    for(int i=pos; i < this->size-1; i++)
      {
	this->data[i] = this->data[i+1];
      }
    this->size--;
  }

  int getSize()
  {
    return this->size;
  }

  friend std::ostream& operator <<(std::ostream& out, List<T, maxSize> mylist)
  {
    out << "[";
    for(int i = 0; i< mylist.size; i++)
      {
	out << mylist.data[i];
	if(i+1 < mylist.size)
	  out << ",";
      }
    out << "]";
    return out;
  }

  
};

int main()
{

  List<int,10> l;
  l.add(1);
  l.add(2);
  l.add(3);
  std::cout<<l<<"\n";
  //removes 1
  l.remove(0);
  std::cout<<l<<"\n";
  std::cout<<l.getSize()<<"\n";
  
  List<int,10> squares;
  for(int i=0;i<10;i++){
	squares.add(i*i);
	//squares.remove(3);
	std :: cout << "Squares items: " << squares << " \n " ;
   }
return 0;
  

}